/*----------------------------------------------------------------------------*/
/* Copyright (c) FIRST 2008. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package edu.wpi.first.wpilibj.templates;


import edu.wpi.first.wpilibj.Jaguar;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.SimpleRobot;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Timer;




public class RobotTemplate extends SimpleRobot 
{
    Jaguar FLJag = new Jaguar(1);
    Jaguar FRJag = new Jaguar(2);
    Jaguar BLJag = new Jaguar(3);
    Jaguar BRJag = new Jaguar(4);
    
    Jaguar shooter = new Jaguar(5);
    
    Solenoid flagin = new Solenoid(1);
    Solenoid flagout = new Solenoid(2);
    Solenoid golfin = new Solenoid(3);
    Solenoid golfout = new Solenoid(4);
    
    Joystick joy1 = new Joystick(1);
    Joystick joy2 = new Joystick(2);
    Joystick joy3 = new Joystick(3);
    
    
      double tank = joy1.getY();
      double turn = joy1.getX();
      double strafe = joy2.getX();
     
    
    
    public void autonomous() 
    {
        
    }

  
         public void operatorControl()
         {
        
            FLJag.set(tank+turn+strafe);
            FRJag.set(-tank+turn+strafe);
            BLJag.set(tank+turn-strafe);
            BRJag.set(-tank+turn-strafe);
            
             if (joy3.getTrigger()== true);
                 {
                   golfout.set(true);
                   golfin.set(false);
                   Timer.delay(.2);
                   golfout.set(false);
                   golfin.set(true);
                   
                 } 
                      
              if (joy1.getTrigger() == true)
                    {
                         if ( flagout.get() == true ) 
                         {
                            flagout.set(false);
                            flagin.set(true);
                         }
                                else 
                                {
                                  flagout.set(true);
                                  flagin.set(false);
                                }
                    }
            
        
              if (joy3.getTop() == true)
              {
                  shooter.set(.3);
              }
              
                

         }
}
