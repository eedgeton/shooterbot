/*----------------------------------------------------------------------------*/
/* Copyright (c) FIRST 2008. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package edu.wpi.first.wpilibj.templates;


import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Jaguar;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.SimpleRobot;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Victor;


/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the SimpleRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class RobotTemplate extends SimpleRobot {
    
    Jaguar FLJag = new Jaguar(1);
    Jaguar FRJag = new Jaguar(2);
    Jaguar BLJag = new Jaguar(3);
    Jaguar BRJag = new Jaguar(4);
    Jaguar TUJag = new Jaguar(7);
    
    Victor belt = new Victor(5);
    Jaguar shooter = new Jaguar(6);
    
    Solenoid flagin = new Solenoid(1);
    Solenoid flagout = new Solenoid(2);
    Solenoid golfin = new Solenoid(8);
    Solenoid golfout = new Solenoid(7);
    
    Joystick joy1 = new Joystick(1);
    Joystick joy2 = new Joystick(2);
    Joystick joy3 = new Joystick(3);
    
    Helpers help = new Helpers(); 
    
    DriverStation kkStation = new DriverStation();
    
    public double SPEED_CURVE = kkStation.getAnalogIn(1);
    
    public double DEAD_ZONE = kkStation.getAnalogIn(2);
    
    DriverStation driverStation = DriverStation.getInstance();
  
     
    Compressor compress = new Compressor(1,1);
      
      
    public void autonomous() {
        
    }

    /**
     * This function is called once each time the robot enters operator control.
     */
    public void operatorControl() {
     
        
        
        boolean lastjoytrigger = joy1.getTrigger();
        
        
        while ( isOperatorControl() && isEnabled()   )
        {

        compress.start();
        
        TUJag.set((joy3.getX())*.5);
        
        double tank = help.powf(joy1.getY(), SPEED_CURVE);
        double turn = help.powf(joy1.getX(), SPEED_CURVE);
        double strafe = help.powf(joy2.getX(), SPEED_CURVE);
        
        
         //Dead zone
        if( tank  < DEAD_ZONE &&  tank  > -DEAD_ZONE)
             tank  = 0;
        
        if(turn < DEAD_ZONE && turn > DEAD_ZONE)
           turn = 0;
        
        if(strafe < DEAD_ZONE && strafe > DEAD_ZONE)
            strafe= 0;
       
      
        
        FLJag.set(-tank+turn+strafe);
        FRJag.set(tank+turn+strafe);
        BLJag.set(-tank+turn-strafe);
        BRJag.set(tank+turn-strafe);
            
             if (joy3.getTrigger()== true)
                 {
                   golfout.set(true);
                   golfin.set(false);
                 }
                   
             if (joy3.getTrigger()== false)
             {
                   golfout.set(false);
                   golfin.set(true);
                   
             } 
                      
              if (joy1.getTrigger() == true && lastjoytrigger == false )
                    {
                         if ( flagout.get() == true ) 
                         {
                            flagout.set(false);
                            flagin.set(true);
                         }
                                else 
                                {
                                  flagout.set(true);
                                  flagin.set(false);
                                }
                    }
            
              
        
              if (joy1.getTop() == true && joy1.getRawButton(3)== false)
              {
                  belt.set(.2);
              }
              
              
              if (joy1.getRawButton(3)== true && joy1.getTop() == false)
              {
                  belt.set(-.2);
              }
             
              if(joy1.getTop() == false && joy1.getRawButton(3) == false)
              {
                  belt.set(0);
              }
          
              if (joy3.getTop() == true)
              {
                  shooter.set(driverStation.getAnalogIn(1));
              }
              
             else 
              {
                  shooter.set(0);
              }
                 lastjoytrigger = joy1.getTrigger();
                 
                 
                 
        
        }
        
    }
    
   
    public void test() {
    
    }
}
